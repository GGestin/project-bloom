using UnityEngine;
using Vuforia;

public class UIManagerChickenCurry : MonoBehaviour
{
    private bool isPaused = false;
    public VuforiaBehaviour vufuria;

    public GameObject onionv2_tip;
    public GameObject onionv2_shadow_tip;
    public GameObject flameproof_tip;
    public GameObject flameproof_shadow_tip;

    public void Awake()
    {
        onionv2_tip.SetActive(false);
        flameproof_tip.SetActive(false);
        if(onionv2_shadow_tip!= null && flameproof_shadow_tip!= null){
            flameproof_shadow_tip.SetActive(false);
            onionv2_shadow_tip.SetActive(false);
        }
    }

    public void PauseCamera()
    {
        isPaused = !isPaused;
        vufuria.enabled = isPaused;
    }

    public void ShowTipsOnionsV2()
    {
        onionv2_tip.SetActive(!onionv2_tip.activeSelf);
        if(onionv2_shadow_tip!= null){
            onionv2_shadow_tip.SetActive(!onionv2_shadow_tip.activeSelf);
        }

    }

    public void ShowTipsFlamproof()
    {
        flameproof_tip.SetActive(!flameproof_tip.activeSelf);
        if (flameproof_shadow_tip != null){
            flameproof_shadow_tip.SetActive(!flameproof_shadow_tip.activeSelf);
        }
    }
}

