using UnityEngine;
using System;
using Vuforia;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Serializable]
    public enum Ricetta
    {
        Ricetta1,
        Ricetta2
    }

    [Serializable]
    public enum TipsType
    {
        R1_Ramekin,
        R1_Onion,
        R1_Fry,
        R2_Onion,
        R2_Flame
    }

    [Header("Simulation")]
    public VuforiaBehaviour vufuria;
    public GameObject current_simulations;
    private bool isPaused = false;

    [Header("Ricetta 1")]
    public GameObject[] R1_simulations;

    public GameObject R1_onion_tip;
    public GameObject R1_ramekins_tip;
    public GameObject R1_fry_tip;

    public GameObject SelectUI1;

    public GameObject TargetR1;
    
    [Header("Ricetta 2")]
    public GameObject[] R2_simulations;

    public GameObject[] R2_onion;
    public GameObject[] R2_flame;

    public GameObject SelectUI2;
    
    public GameObject TargetR2;

    public void Awake()
    {
        R1_onion_tip.SetActive(false);
        R1_ramekins_tip.SetActive(false);
        R1_fry_tip.SetActive(false);
        SelectUI1.SetActive(false);
        foreach (var item in R1_simulations)
        {
            item.SetActive(false);
        }

        foreach (var item in R2_onion)
        {
            item.SetActive(false);
        }

        foreach (var item in R2_flame)
        {
            item.SetActive(false);
        }

        foreach (var item in R2_simulations)
        {
            item.SetActive(false);
        }
        SelectUI2.SetActive(false);
    }

    public void RAR_PauseCamera()
    {
        vufuria.enabled = isPaused;
        isPaused = !isPaused;
    }

    public void RAR_Quit()
    {
        Application.Quit();
    }

    public void RAR_Rate()
    {
        Application.OpenURL("https://docs.google.com/forms/d/e/1FAIpQLSeSioNzHc-eCVj6VcLWmLLnSm3_lEMzlkr42SZNzoQSJkbI0Q/viewform");
    }

    public void RAR_ToggleTips(int r, int t, int n)
    {
        Debug.Log("Prout");
        switch ((Ricetta) r)
        {
            case Ricetta.Ricetta1:
                switch ((TipsType) t)
                {
                    case TipsType.R1_Ramekin:
                        R1_ramekins_tip.SetActive(!R1_ramekins_tip.activeSelf);
                        break;
                    case TipsType.R1_Onion:
                        R1_onion_tip.SetActive(!R1_onion_tip.activeSelf);
                        break;
                    case TipsType.R1_Fry:
                        R1_fry_tip.SetActive(!R1_fry_tip.activeSelf);
                        break;
                    default:
                        break;
                }
                break;
            case Ricetta.Ricetta2:
                switch ((TipsType) t)
                {
                    case TipsType.R2_Onion:
                        R2_onion[n].SetActive(!R2_onion[n].activeSelf);
                        break;
                    case TipsType.R2_Flame:
                        R2_flame[n].SetActive(!R2_flame[n].activeSelf);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    public void RAR_ToggleTips_Ricetta1(int g) => RAR_ToggleTips(0, g, 0);
    public void RAR_ToggleTips_Ricetta2(string g) => RAR_ToggleTips(1, int.Parse(g.Split('/')[0]), int.Parse(g.Split('/')[1]));

    public void RAR_ToggleSelectUI(int r, bool state)
    {
        switch ((Ricetta)r)
        {
            case Ricetta.Ricetta1:
                SelectUI1.SetActive(state);
                break;
            case Ricetta.Ricetta2:
                SelectUI2.SetActive(state);
                break;
            default:
                break;
        }
    }
    public void RAR_ToggleSelectUI_Ricetta1(bool state) => RAR_ToggleSelectUI(0, state);
    public void RAR_ToggleSelectUI_Ricetta2(bool state) => RAR_ToggleSelectUI(1, state);

    public void RAR_SelectSimulation(int r, int n)
    {
        switch ((Ricetta)r)
        {
            case Ricetta.Ricetta1:
                if (current_simulations != null)
                    current_simulations.SetActive(false);
                current_simulations = R1_simulations[n];
                current_simulations.SetActive(true);
                R1_onion_tip.SetActive(false);
                R1_ramekins_tip.SetActive(false);
                R1_fry_tip.SetActive(false);
                break;
            case Ricetta.Ricetta2:
                if (current_simulations != null)
                    current_simulations.SetActive(false);
                current_simulations = R2_simulations[n];
                current_simulations.SetActive(true);
                R2_onion[n].SetActive(false);
                R2_flame[n].SetActive(false);
                break;
            default:
                break;
        }
    }
    public void RAR_SelectSimulation_Ricetta1(int n) => RAR_SelectSimulation(0, n);
    public void RAR_SelectSimulation_Ricetta2(int n) => RAR_SelectSimulation(1, n);
}
