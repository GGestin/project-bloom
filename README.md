# HCI Project : RicettAR

##### Group : “The Frenchies”,
##### [Alan Vadeleau](https://gitlab.com/aVadeleau), [Gaëtan Gestin](https://ggestin.com), [Chiara Relevat](https://gitlab.com/Chiara06830)
##### Supervisor : Andrea Giachetti
##### Year 2022/2023

## Table of Content
  - [1. Quickstart](#1-quickstart)
  - [2. Theme: Cooking Tips in AR](#2-theme-cooking-tips-in-ar)
  - [3. Tested features](#3-tested-features)
    - [a. Buttons design](#a-buttons-design)
    - [b. Tips design](#b-tips-design)
    - [c. Quality of Experiment](#c-quality-of-experiment)
  - [4. Testing plan](#4-testing-plan)
  - [5. How to use the app](#5-how-to-use-the-app)

## Key Points:
Vuforia(AR), Unity, Evaluate Interaction Design options

## 1. Quickstart
You can download the app on your Android Phone here:

[<img src="./Img/QRCode.png" width="200">](./Img/QRCode.png)
Or with this [link](./Ricett'AR.apk).

And you can test thet design on the two recipes :
| Button design with Gnocchis | Tips design with Chicken Curry |
| --------------------------- | ------------------------------ |
[<img src="./Assets/SamplesResources/InstantImageTarget/ricettAR.jpg" width="200">](./Assets/SamplesResources/InstantImageTarget/ricettAR.jpg) | [<img src="./Assets/SamplesResources/InstantImageTarget/ricettAR_chiken_curry.jpg" width="200">](./Assets/SamplesResources/InstantImageTarget/ricettAR_chiken_curry.jpg)

## 2. Theme: Cooking Tips in AR
This project implement a way to enhance cooking recipes with AR . The user will have a printed recipe which he can simply use as it is, but he could also enhance his experience with the application that will show tips about ingredients, tools, and techniques on the phone.

## 3. Tested features
The point here is to test differents possible design to determine wich one is the best.

Here is the link to the [questionnaire](https://forms.gle/ziwyGUM5g4xJYR439) to fill after testing all the designs.

After the data gathering we will answer questions like :

-   Is X design viable ?
-   Is it accessible ?
-   Which alternative is overall the best ?
-   What are the strengths and the weaknesses ?

### a. Buttons design
We implemented 3 buttons design:
| Simple                  | Outline                 | Overlay                 |
| ----------------------- | ----------------------- | ----------------------- |
| ![](./Img/button_1.jpg) | ![](./Img/button_2.jpg) | ![](./Img/button_3.jpg) |

Each design is evaluated with 3 questions:
| Question                              | Minimum grade                  | Maximum grade           |
| ------------------------------------- | ------------------------------ | ----------------------- |
| How quickly did you see the buttons ? | 1. I didn't see them           | 5. Immediatly           |
| Was it easy to click on the buttons ? | 1. I miss buttons all the time | 5. I never miss a click |
| Was it intuitive ?                    | 1. Not understandable at all   | 5. Very intuitive       |


### b. Tips design
We implemented 2 color of tips:
| Transparent blue background with white text | Opaque white background with black text |
| ------------------------------------------- | --------------------------------------- |
| ![](./Img/tips_onion_1.png)                 | ![](./Img/tip_onion_2.png)              |

And on this second color we implemented 3 ways of displaying tips:
| Besides the button   | Centered on the recipe | Over all the recipe  |
| -------------------- | ---------------------- | -------------------- |
| ![](./Img/tip_1.png) | ![](./Img/tip_2.png)   | ![](./Img/tip_3.png) |

Each design is evaluated with 3 questions:
| Question                             | Minimum grade    | Maximum grade           |
| ------------------------------------ | ---------------- | ----------------------- |
| Was it readable ?                    | 1. Not at all    | 5. Immediatly readable  |
| Are you satisfied by the rendering ? | 1. No, it's ugly | 5. Yes, it's beautifull |

### c. Quality of Experiment
We also want to evaluate the global quality of experiment to know if our application is viable.

It is done with those questions:
| Question                          | Minimum grade                    | Maximum grade                      |
| --------------------------------- | -------------------------------- | ---------------------------------- |
| Was it readable ?                 | 1. Not at all                    | 5. Immediatly readable             |
| Did you find this app useful ?    | 1. No, I will never use it again | 5. Yes, I will use it all the time |
| Did you find this app satisfying? | 1. It was frustrating            | 5. Very satisfying                 |

## 4. Testing plan
The plan is to test these feature on:
- 20 to 30 individuals
- age 18 to 68
- Don't work in the IT domain
- From different Cultures
- People used/Not used to AR

## 5. How to use the app
1. Open the app
1. Scan one of the 2 “Ricetta” available
1. Test all experiments (button with numbers on the top)
1. Click the “Star” button to answer the questionnaire

![](./Img/tuto.png)
